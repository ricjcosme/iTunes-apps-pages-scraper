# Scraper for the Appstore Apps

- `git clone https://github.com/ricjcosme/iTunes-apps-pages-scraper.git`
- `cd iTunes-apps-pages-scraper`
- `pip install -r requirements.txt`
- `python scraper.py input.csv`

And find the two newly created files in the same folder:
- apps.json
- filtered_apps.json
