import os.path
from helpers import *
import csv
import requests
import json
from bs4 import BeautifulSoup

# logger init
logger = logging.getLogger(__name__)
logger_conf(logger)

# argparser init
args = parser.parse_args()

apps = []
apps_in_spanish_and_tagalog = []
apps_with_insta_in_name = []


def scrape(row):
	# here we go
	# is /us/ in the URL?
	url_split = row[1].split('/')
	if 'us' not in url_split:
		logger.info('Not scraping %s - %s: not an United States page' % (row[0], row[1]))
		return

	name = row[0]
	app_identifier = row[1].rsplit('/', 1)[1].replace('id', '')

	# insta in name?
	if 'insta'.upper() in name.upper():
		apps_with_insta_in_name.append(int(app_identifier))

	# let's try to fetch the html and scrape the remaining items
	try:
		r = requests.get(row[1])

		soup = BeautifulSoup(r.text, 'html.parser')
		json_doc = json.loads(soup.find("script", id='shoebox-ember-data-store').text)

		# language list
		languages = json_doc['data']['attributes']['softwareInfo']['languagesDisplayString'].split(", ")

		# Spanish and Tagalog?
		if 'Spanish' and 'Tagalog' in languages:
			apps_in_spanish_and_tagalog.append(int(app_identifier))

		minimum_version = json_doc['data']['attributes']['softwareInfo']['requirementsString']

		apps_obj = {
			'app_identifier': int(app_identifier),
			'languages': sorted(languages),
			'name': name,
			'minimum_version': re.findall("\d+\.\d+", minimum_version)[0]
		}
		apps.append(apps_obj)
		logger.info('Scraped %s' % name)

	except requests.RequestException as e:
		logger.error('Error fetching %s: %s' % (row[1], e))


def main():
	# let's check if we have a parameter
	if args.csv_file == 'check_empty':
		logger.error("Please input the csv_file parameter")
		exit(1)

	# let's check if the parameter is a valid file
	if not os.path.isfile(args.csv_file):
		logger.error("The csv_file parameter is not a file")
		exit(1)

	# let's see if the file is valid csv otherwise stop and exit
	csv_file = args.csv_file

	with open(csv_file, 'rb') as f:
		reader = csv.reader(f)
		# ignore header
		next(reader, None)

		try:
			# let's see if this csv has the expected format
			for row in reader:
				if len(row) > 2:
					logger.error('Error in file %s, line %d: wrong field count' %
									(csv_file, reader.line_num))
					exit(1)
				if re.match(regex, row[1]) is None:
					logger.error('Error in file %s, line %d: %s is not a valid URL' %
									(csv_file, reader.line_num, row[1]))
					exit(1)

				# if we got here we're all set to start scraping
				# let's call the scrape function
				scrape(row)

		except csv.Error as e:
			logger.error('Error in file %s, line %d: %s' %
							(csv_file, reader.line_num, e))
			exit(1)

	filtered_apps = {
		'apps_in_spanish_and_tagalog': sorted(apps_in_spanish_and_tagalog),
		'apps_with_insta_in_name': sorted(apps_with_insta_in_name)
	}

	with open(apps_json, 'w') as outfile:
		json.dump(apps, outfile)
	logger.info('Wrote apps.json')

	with open(filtered_apps_json, 'w') as outfile:
		json.dump(filtered_apps, outfile)
	logger.info('Wrote filtered_apps.json')


if __name__ == "__main__":
	main()

























