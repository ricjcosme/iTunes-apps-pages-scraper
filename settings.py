import logging, argparse, os

# logger
log_format = '%(asctime)s - %(levelname)s - %(message)s'
log_level = logging.INFO

# argparse
parser = argparse.ArgumentParser(description='Scraper')
parser.add_argument("csv_file", nargs='?', default="check_empty")

# files
abs_path = os.path.abspath(__file__).rsplit('/', 1)[0]
apps_json = ''.join([abs_path, '/apps.json'])
filtered_apps_json = ''.join([abs_path, '/filtered_apps.json'])